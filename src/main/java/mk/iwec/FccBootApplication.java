package mk.iwec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FccBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(FccBootApplication.class, args);
	}

}
